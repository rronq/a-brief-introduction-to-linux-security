# A Brief Introduction to Linux Security Hardening

A short manual for introducing students to GNU/Linux security. Within this manual you'll find the following topics:
- Managing Users and groups
- Password Security
- Basic Networking
- Using Uncomplicated Firewall
- User, Group, and File Permission
- The Basics of Securing Key Services
- Updating and Upgrading the System

There's also an additional chapter that provides links to extra resources. Some students may find this guide especially useful in blue team competitions like CyberPatriot, Hivestorm, or the Collegiate Cyber Defense Competition.
